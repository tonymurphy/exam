package uk.co.wowcher.exam;

public class FileUploadStatusVO {

    private boolean zipFile;
    private boolean uploadSuccessful;
    private String filename;
    private String zipFileName;
    private String errorMsg;
    
    /**
     * Constructor.
     */
    public FileUploadStatusVO() {
    }


    /**
     * Constructor.
     * 
     * @param filename
     * @param zipFileName if zipFile set to true, then this should be set
     * @param zipFile true if file uploaded was a zipfile
     * @param uploadSuccessful true if upload was successful
     */
    public FileUploadStatusVO(String filename, String zipFileName, boolean zipFile, boolean uploadSuccessful) {
        this();
        
        this.filename = filename;
        this.zipFileName = zipFileName;
        this.zipFile = zipFile;
        this.uploadSuccessful = uploadSuccessful;
    }


    /**
     * Constructor for non-zip file uploads.
     * 
     * @param filename
     * @param uploadSuccessful true if upload was successful
     */
    public FileUploadStatusVO(String filename, boolean uploadSuccessful) {
        this(filename, null, false, uploadSuccessful);
    }

    
    /**
     * Gets the zipFile.
     * 
     * @return Returns the zipFile.
     */
    public boolean isZipFile() {
        return zipFile;
    }


    /**
     * Sets the zipFile.
     * 
     * @param zipFile The zipFile to set.
     */
    public void setZipFile(boolean zipFile) {
        this.zipFile = zipFile;
    }


    /**
     * Gets the uploadSuccessful.
     * 
     * @return Returns the uploadSuccessful.
     */
    public boolean isUploadSuccessful() {
        return uploadSuccessful;
    }


    /**
     * Sets the uploadSuccessful.
     * 
     * @param uploadSuccessful The uploadSuccessful to set.
     */
    public void setUploadSuccessful(boolean uploadSuccessful) {
        this.uploadSuccessful = uploadSuccessful;
    }


    /**
     * Gets the filename.
     * 
     * @return Returns the filename.
     */
    public String getFilename() {
        return filename;
    }


    /**
     * Sets the filename.
     * 
     * @param filename The filename to set.
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }


    /**
     * Gets the zipFileName.
     * 
     * @return Returns the zipFileName.
     */
    public String getZipFileName() {
        return zipFileName;
    }


    /**
     * Sets the zipFileName.
     * 
     * @param zipFileName The zipFileName to set.
     */
    public void setZipFileName(String zipFileName) {
        this.zipFileName = zipFileName;
    }


    /**
     * Gets the errorMsg.
     * 
     * @return Returns the errorMsg.
     */
    public String getErrorMsg() {
        return errorMsg;
    }


    /**
     * Sets the errorMsg.
     * 
     * @param errorMsg The errorMsg to set.
     */
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

}
