/**
 * 
 */
package uk.co.wowcher.exam;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 */
public class FileUploadHelper {

	
    public List<FileUploadStatusVO> uploadImages(final FileUploadForm form, int position) {

        final List<FileUploadStatusVO> uploadStatusList = new ArrayList<FileUploadStatusVO>();
        final int numberOfFiles = form.getFiles().size();
        for (int i = 0; i < numberOfFiles; i++) {
            final MultipartFile multipartFile = form.getFiles().get(i);
            final String filename = multipartFile.getOriginalFilename();

            if (multipartFile != null && !StringUtils.isEmpty(filename)) {
                final String altTag = getAltTagOrCaption(form.getUseFilenameAsAltTagFlags(), filename, form.getAltTags().get(i), i);
                final String caption = getAltTagOrCaption(form.getUseFilenameAsCaptionFlags(), filename, form.getCaptions().get(i), i);
                FileUploadStatusVO fileUploadStatusVO = null;
                try {
                    fileUploadStatusVO = saveAndSendImageToS3(multipartFile.getSize(), filename, multipartFile.getInputStream(), altTag, caption, position);
                } catch (final IOException e) {
                    fileUploadStatusVO = new FileUploadStatusVO(filename, "", false, false);
                }
                uploadStatusList.add(fileUploadStatusVO);
                position++;
            }

        }
        return uploadStatusList;
    }

	
    private String getAltTagOrCaption(List<Boolean> flags, String filename, String alternativeString, int index) {
        if (flags.size() <= index || flags.get(index) == null || flags.get(index).booleanValue() == false) {
            return alternativeString;
        }
        return filename.substring(0, filename.indexOf("."));
    }

    
    /**
     * 
     * @param filesize
     * @param filename
     * @param in
     * @param altTag
     * @param caption
     * @param position
     */
    private FileUploadStatusVO saveAndSendImageToS3(long filesize, String filename, InputStream in, String altTag, String caption, int position) {
    	return new FileUploadStatusVO();
    }
	
}
