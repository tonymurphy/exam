package uk.co.wowcher.exam;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * File upload bean.
 */
public class FileUploadForm {

	private List<MultipartFile> files = new ArrayList<MultipartFile>();
    private List<String> names = new ArrayList<String>();

    private List<String> altTags;
    private List<Boolean> useFilenameAsAltTagFlags;

    private List<String> captions;
    private List<Boolean> useFilenameAsCaptionFlags;


    /**
     * Set names.
     * 
     * @param names
     */
    public void setNames(List<String> names) {
        this.names = names;
    }


    /**
     * Set multipart files.
     * 
     * @param files {@link MultipartFile}s
     */
    public void setFiles(List<MultipartFile> files) {
        this.files = files;
    }


    /**
     * Gets the files.
     * 
     * @return Returns the files.
     */
    public List<MultipartFile> getFiles() {
        return files;
    }


    /**
     * Gets the names.
     * 
     * @return Returns the names.
     */
    public List<String> getNames() {
        return names;
    }


    /**
     * @return the altTags
     */
    public List<String> getAltTags() {
        return altTags;
    }


    /**
     * @param altTags the altTags to set
     */
    public void setAltTags(List<String> altTags) {
        this.altTags = altTags;
    }


    /**
     * @return the useFilenameAsAltTagFlags
     */
    public List<Boolean> getUseFilenameAsAltTagFlags() {
        return useFilenameAsAltTagFlags;
    }


    /**
     * @param useFilenameAsAltTagFlags the useFilenameAsAltTagFlags to set
     */
    public void setUseFilenameAsAltTagFlags(List<Boolean> useFilenameAsAltTagFlags) {
        this.useFilenameAsAltTagFlags = useFilenameAsAltTagFlags;
    }


    /**
     * @return the captions
     */
    public List<String> getCaptions() {
        return captions;
    }


    /**
     * @param captions the captions to set
     */
    public void setCaptions(List<String> captions) {
        this.captions = captions;
    }


    /**
     * @return the useFilenameAsCaptionFlags
     */
    public List<Boolean> getUseFilenameAsCaptionFlags() {
        return useFilenameAsCaptionFlags;
    }


    /**
     * @param useFilenameAsCaptionFlags the useFilenameAsCaptionFlags to set
     */
    public void setUseFilenameAsCaptionFlags(List<Boolean> useFilenameAsCaptionFlags) {
        this.useFilenameAsCaptionFlags = useFilenameAsCaptionFlags;
    }

}
